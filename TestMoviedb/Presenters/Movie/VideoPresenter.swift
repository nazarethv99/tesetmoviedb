//
//  VideoPresenter.swift
//  TestMoviedb
//
//  Created by Hostienda on 5/29/19.
//  Copyright © 2019 Hostienda. All rights reserved.
//

import Foundation

protocol videoDelegate: class {
    func success(result: [ResultsVideo])
    func ket(trailer: String)
    func error(error : String)
}

class VideoPresenter {
    var delegate : videoDelegate?
    
    init(delegate: videoDelegate) {
        self.delegate = delegate
    }
    
    func getVideoDetail(id : Int){
        MovieService.getVideoMovie(id: id, successBlock: {
            result in
            
            var idKet = ""
            if let array = result.results {
                
                if let tempo = result.results{
                    for temp in tempo{
                        if temp.type == "Trailer"{
                            idKet = temp.key ?? ""
                        }
                    }
                }
                self.delegate?.ket(trailer: idKet)
                self.delegate?.success(result: array)
            } else {
                self.delegate?.error(error: "No available")
            }
        }, errorBlock: {
            error in
            self.delegate?.error(error: error)
        })
    }
    
}

//
//  MoviePresenter.swift
//  TestMoviedb
//
//  Created by Hostienda on 5/27/19.
//  Copyright © 2019 Hostienda. All rights reserved.
//

import Foundation

protocol movieDelegate: class {
    func success(result: [Results])
    func error(error : String)
}

class MoviePresenter {
    var delegate : movieDelegate?
    
    init(delegate: movieDelegate) {
        self.delegate = delegate
    }
    
    func getPopular(typeMovies : Int){
        MovieService.getPopular(typeMovies : typeMovies ,successBlock: {
            result in
            if let array = result.results {
                self.delegate?.success(result: array)
            } else {
                self.delegate?.error(error: "No available Popular")
            }
        }, errorBlock: {
            error in
            self.delegate?.error(error: error)
        })
    }
}

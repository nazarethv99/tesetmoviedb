//
//  PopularCell.swift
//  TestMoviedb
//
//  Created by Hostienda on 5/27/19.
//  Copyright © 2019 Hostienda. All rights reserved.
//

import UIKit

class PopularCell: UITableViewCell {

    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    static let nib = UINib(nibName: "PopularCell", bundle: nil)

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//
//  UpcomingVC.swift
//  TestMoviedb
//
//  Created by Hostienda on 5/30/19.
//  Copyright © 2019 Hostienda. All rights reserved.
//

import UIKit
import PKHUD
import SDWebImage

class UpcomingVC: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    
    var arrayUpcomingMovies : [Results]?
    var presenter : MoviePresenter?
    
    var customSearchController: CustomSearchController?
    
    var filteredArray : [Results]?
    var shouldShowSearchResults = false
    
    var apiImageType = Api.image.image500
    var typeMovies = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(PopularCell.nib, forCellReuseIdentifier: "PopularCell")
        presenter = MoviePresenter(delegate: self)
        HUD.show(.progress)
        presenter?.getPopular(typeMovies : typeMovies)
        configureCustomSearchController()
    }
    
    func configureCustomSearchController() {
        
        customSearchController = CustomSearchController(searchResultsController: self, searchBarFrame: CGRect(x: 0.0, y: 0.0, width: tableView.frame.size.width, height: 50.0), searchBarFont: UIFont(name: "Futura", size: 16.0)!, searchBarTextColor: UIColor.white, searchBarTintColor: UIColor.darkGray)
        
        customSearchController?.customSearchBar.placeholder = "Search in the Upcoming list ..."
        
        customSearchController?.customDelegate = self
    }
    
}

extension UpcomingVC: UISearchBarDelegate, CustomSearchControllerDelegate {
    func didStartSearching() {
        tableView.reloadData()
    }
    
    func didTapOnSearchButton() {
        if !shouldShowSearchResults{
            shouldShowSearchResults = true
            tableView.reloadData()
        }
    }
    
    func didTapOnCancelButton() {
        shouldShowSearchResults = false
        tableView.reloadData()
    }
    
    func didChangeSearchText(_ searchText: String) {
        filteredArray = arrayUpcomingMovies!.filter({ (exercise) -> Bool in
            let exerciseText: NSString = ((exercise.title ?? "Name") as NSString )
            shouldShowSearchResults = true
            return (exerciseText.range(of: searchText, options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
        })
        if searchText == "" {
            filteredArray = arrayUpcomingMovies
            tableView.reloadData()
        }
        tableView.reloadData()
    }
    
    
}

extension UpcomingVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if shouldShowSearchResults{
            return filteredArray?.count ?? 0
        } else {
            guard let count = arrayUpcomingMovies?.count else {
                return 0
            }
            return count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PopularCell") as? PopularCell
        
        if shouldShowSearchResults{
            cell?.lblTitle.text = (filteredArray?[indexPath.row].title)?.firstUppercased
            cell?.lblDate.text = filteredArray?[indexPath.row].release_date
            cell?.lblRating.text = " \(filteredArray?[indexPath.row].vote_average ?? 0)"
            
            let imgUrl = filteredArray?[indexPath.row].poster_path ?? ""
            let imgPath = "\(apiImageType)\(imgUrl)"
            
            if let url = URL(string: imgPath) {
                cell?.loading.startAnimating()
                cell?.imgPoster.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "j3"), options: SDWebImageOptions(rawValue: 0),
                                           completed: { image, error, cacheType, imageURL in
                                            cell?.loading.stopAnimating()
                                            cell?.imgPoster.image = image
                })
            } else {
                cell?.loading.stopAnimating()
                cell?.imgPoster.image = #imageLiteral(resourceName: "j3")
            }
        } else {
            cell?.lblTitle.text = (arrayUpcomingMovies?[indexPath.row].title)?.firstUppercased
            cell?.lblDate.text = arrayUpcomingMovies?[indexPath.row].release_date
            cell?.lblRating.text = " \(arrayUpcomingMovies?[indexPath.row].vote_average ?? 0)"
            
            let imgUrl = arrayUpcomingMovies?[indexPath.row].poster_path ?? ""
            let imgPath = "\(apiImageType)\(imgUrl)"
            
            if let url = URL(string: imgPath) {
                cell?.loading.startAnimating()
                cell?.imgPoster.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "j3"), options: SDWebImageOptions(rawValue: 0),
                                           completed: { image, error, cacheType, imageURL in
                                            cell?.loading.stopAnimating()
                                            cell?.imgPoster.image = image
                })
            } else {
                cell?.loading.stopAnimating()
                cell?.imgPoster.image = #imageLiteral(resourceName: "j3")
            }
        }
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let position = indexPath.row
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailMoviesVC") as! DetailMoviesVC
        vc.modalPresentationStyle = .overCurrentContext
        
        if shouldShowSearchResults{
            vc.resultVC = self.filteredArray?[position]
        } else {
            vc.resultVC = self.arrayUpcomingMovies?[position]
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.customSearchController?.customSearchBar
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.customSearchController?.customSearchBar.frame.height ?? 10.0
    }
    
}

extension UpcomingVC: movieDelegate {
    func success(result: [Results]) {
        HUD.hide()
        
        self.arrayUpcomingMovies = result
        
        self.tableView.reloadData()
    }
    
    func error(error: String) {
        self.arrayUpcomingMovies = Storage.movieInfo?.results
        self.tableView.reloadData()
        HUD.hide()
        print(error)
    }
}

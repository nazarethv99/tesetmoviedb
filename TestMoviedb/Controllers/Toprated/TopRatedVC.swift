//
//  TopRated.swift
//  TestMoviedb
//
//  Created by Hostienda on 5/29/19.
//  Copyright © 2019 Hostienda. All rights reserved.
//

import UIKit
import PKHUD
import SDWebImage

class TopRatedVC: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    
    var arrayTopratedMovies : [Results]?
    var presenter : MoviePresenter?
    
    var customSearchController: CustomSearchController?
    
    var filteredArray : [Results]?
    var shouldShowSearchResults = false
    
    var apiImageType = Api.image.image500
    var typeMovies = 2
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(PopularCell.nib, forCellReuseIdentifier: "PopularCell")
        presenter = MoviePresenter(delegate: self)
        HUD.show(.progress)
        presenter?.getPopular(typeMovies : typeMovies)
        configureCustomSearchController()
    }
    
    func configureCustomSearchController() {
        
        customSearchController = CustomSearchController(searchResultsController: self, searchBarFrame: CGRect(x: 0.0, y: 0.0, width: tableView.frame.size.width, height: 50.0), searchBarFont: UIFont(name: "Futura", size: 16.0)!, searchBarTextColor: UIColor.white, searchBarTintColor: UIColor.darkGray)
        
        customSearchController?.customSearchBar.placeholder = "Search in the Top Rated list ...".localized
        
        customSearchController?.customDelegate = self
    }
    
}

extension TopRatedVC: UISearchBarDelegate, CustomSearchControllerDelegate {
    func didStartSearching() {
        tableView.reloadData()
    }
    
    func didTapOnSearchButton() {
        if !shouldShowSearchResults{
            shouldShowSearchResults = true
            tableView.reloadData()
        }
    }
    
    func didTapOnCancelButton() {
        shouldShowSearchResults = false
        tableView.reloadData()
    }
    
    func didChangeSearchText(_ searchText: String) {
        filteredArray = arrayTopratedMovies?.filter({ (exercise) -> Bool in
            let exerciseText: NSString = ((exercise.title ?? "Name") as NSString )
            shouldShowSearchResults = true
            return (exerciseText.range(of: searchText, options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
        })
        if searchText == "" {
            filteredArray = arrayTopratedMovies
            tableView.reloadData()
        }
        tableView.reloadData()
    }
    
    
}

extension TopRatedVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if shouldShowSearchResults{
            return filteredArray?.count ?? 0
        } else {
            guard let count = arrayTopratedMovies?.count else {
                return 0
            }
            return count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PopularCell") as? PopularCell
        
        if shouldShowSearchResults{
            cell?.lblTitle.text = (filteredArray?[indexPath.row].title)?.firstUppercased
            cell?.lblDate.text = filteredArray?[indexPath.row].release_date
            cell?.lblRating.text = " \(filteredArray?[indexPath.row].vote_average ?? 0)"
            
            let imgUrl = filteredArray?[indexPath.row].poster_path ?? ""
            let imgPath = "\(apiImageType)\(imgUrl)"
            
            if let url = URL(string: imgPath) {
                cell?.loading.startAnimating()
                cell?.imgPoster.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "j3"), options: SDWebImageOptions(rawValue: 0),
                                           completed: { image, error, cacheType, imageURL in
                                            cell?.loading.stopAnimating()
                                            cell?.imgPoster.image = image
                })
            } else {
                cell?.loading.stopAnimating()
                cell?.imgPoster.image = #imageLiteral(resourceName: "j3")
            }
        } else {
            cell?.lblTitle.text = (arrayTopratedMovies?[indexPath.row].title)?.firstUppercased
            cell?.lblDate.text = arrayTopratedMovies?[indexPath.row].release_date
            cell?.lblRating.text = " \(arrayTopratedMovies?[indexPath.row].vote_average ?? 0)"
            
            let imgUrl = arrayTopratedMovies?[indexPath.row].poster_path ?? ""
            let imgPath = "\(apiImageType)\(imgUrl)"
            
            if let url = URL(string: imgPath) {
                cell?.loading.startAnimating()
                cell?.imgPoster.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "j3"), options: SDWebImageOptions(rawValue: 0),
                                           completed: { image, error, cacheType, imageURL in
                                            cell?.loading.stopAnimating()
                                            cell?.imgPoster.image = image
                })
            } else {
                cell?.loading.stopAnimating()
                cell?.imgPoster.image = #imageLiteral(resourceName: "j3")
            }
        }
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let position = indexPath.row
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailMoviesVC") as! DetailMoviesVC
        vc.modalPresentationStyle = .overCurrentContext
        
        if shouldShowSearchResults{
            vc.resultVC = filteredArray?[position]
        } else {
            vc.resultVC = arrayTopratedMovies?[position]
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.customSearchController?.customSearchBar
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.customSearchController?.customSearchBar.frame.height ?? 10.0
    }
    
}

extension TopRatedVC: movieDelegate {
    func success(result: [Results]) {
        HUD.hide()
        arrayTopratedMovies = result
        tableView.reloadData()
    }
    
    func error(error: String) {
        arrayTopratedMovies = Storage.movieInfo?.results
        tableView.reloadData()
        HUD.hide()
        print(error)
    }
}

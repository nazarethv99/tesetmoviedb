//
//  DetailMoviesVC.swift
//  TestMoviedb
//
//  Created by Hostienda on 5/24/19.
//  Copyright © 2019 Hostienda. All rights reserved.
//

import UIKit
import SDWebImage
import YoutubePlayer_in_WKWebView
import PKHUD

class DetailMoviesVC: UIViewController {

    @IBOutlet weak var imgBackGround: UIImageView!
    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblOverview: UILabel!
    
    @IBOutlet weak var loadingBackground: UIActivityIndicatorView!
    @IBOutlet weak var loadingPoster: UIActivityIndicatorView!
    @IBOutlet weak var loadingVideo: UIActivityIndicatorView!
    
    
    @IBOutlet weak var playerView: WKWebView!
    
  
    var delegateVideo : VideoPresenter?
    var keyTrailer : String?
    var resultVC : Results?
    var arrayVideoMovies : [ResultsVideo]? = []
    var apiImageType = Api.image.image500
    var urlImagePoster : String?
    var urlImageBackground : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let id = resultVC?.id
        delegateVideo = VideoPresenter(delegate: self)
        playerView.navigationDelegate = self
        delegateVideo?.getVideoDetail(id: id ?? 0)
        SetupView()
        LoadImage()
    }
    
    func getVideo(videoCode:String){
        let url = URL(string: "https://www.youtube.com/embed/\(videoCode)")
        playerView.load(URLRequest(url: url!))
        
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func SetupView(){
        lblTitle.text = resultVC?.title
        lblDate.text = resultVC?.release_date
        lblRating.text = " \(resultVC?.vote_average ?? 0)"
        lblOverview.text = resultVC?.overview
        
        urlImagePoster = "\(apiImageType)\(resultVC?.poster_path ?? "")"
        urlImageBackground = "\(apiImageType)\(resultVC?.backdrop_path ?? "")"
    }
    
    func LoadImage(){
        if let urlString = urlImagePoster, let url = URL(string: urlString){
            loadingPoster.startAnimating()
            imgPoster.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "j3poster"),options: SDWebImageOptions(rawValue: 0), completed: {
                image, error, cacheType, imageURL in
                self.loadingPoster.stopAnimating()
                self.imgPoster.image = image
            })
        } else {
            loadingPoster.stopAnimating()
            imgPoster.image = #imageLiteral(resourceName: "j3poster")
        }
        
        if let urlString = urlImageBackground, let url = URL(string: urlString){
            loadingBackground.startAnimating()
            imgBackGround.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "j3"),options: SDWebImageOptions(rawValue: 0), completed: {
                image, error, cacheType, imageURL in
                self.loadingBackground.stopAnimating()
                self.imgBackGround.image = image
            })
        } else {
            loadingBackground.stopAnimating()
            imgBackGround.image = #imageLiteral(resourceName: "j3")
        }
    }
}

extension DetailMoviesVC: videoDelegate {
    func ket(trailer: String) {
        keyTrailer = trailer
        
        getVideo(videoCode: keyTrailer ?? "")
    }
    
    func success(result: [ResultsVideo]) {
        HUD.hide()
        arrayVideoMovies = result
        
    }
    
    func error(error: String) {
        print(error)
    }
}

extension DetailMoviesVC : WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loadingVideo.stopAnimating()
    }
}

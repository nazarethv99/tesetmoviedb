//
//  MovieService.swift
//  TestMoviedb
//
//  Created by Hostienda on 5/27/19.
//  Copyright © 2019 Hostienda. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class MovieService {
    
    class func getPopular(typeMovies : Int , successBlock:@escaping ( _ onSuccess: ListMovie) ->(), errorBlock: @escaping (_ error: String) ->()) {
        
        switch typeMovies {
        case 1 :
            let url = Api.Movie.popularMovies
            
            Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate().responseObject{
                (response: DataResponse<ListMovie>) in
                switch response.result {
                    
                case .success(_):
                    if let result = response.result.value{
                        successBlock(result)
                    } else {
                        errorBlock("Wrong data while getting movies")
                    }
                case .failure(_):
                    errorBlock("Error de conexion getPopular")
                }
            }
            
        case 2 :
            let url = Api.Movie.topRatedMovies
    
            Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate().responseObject{
                (response: DataResponse<ListMovie>) in
                switch response.result {
    
                case .success(_):
                    if let result = response.result.value{
                        successBlock(result)
                    } else {
                        errorBlock("Wrong data while getting movies")
                    }
                case .failure(_):
                    errorBlock("Error de conexion getTopRatedMovie")
                }
            }
        case 3 :
            let url = Api.Movie.upComingMovies
            
            Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate().responseObject{
                (response: DataResponse<ListMovie>) in
                switch response.result {
                    
                case .success(_):
                    if let result = response.result.value{
                        successBlock(result)
                    } else {
                        errorBlock("Wrong data while getting movies")
                    }
                case .failure(_):
                    errorBlock("Error de conexion getUpcomingMovies")
                }
            }
        default:
            print("Error")
        }
    }
    
    class func getVideoMovie(id : Int , successBlock:@escaping ( _ onSuccess: VideoMovie) ->(), errorBlock: @escaping (_ error: String) ->()){
        let firstPart = Api.video.videoDetail
        let restPartUrl = "\(firstPart)\(id)\(Api.video.videoRest)"
        
        Alamofire.request(restPartUrl, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate().responseObject{
            (response: DataResponse<VideoMovie>) in
            switch response.result{
                
            case .success(_):
                if let result = response.result.value{
                    successBlock(result)
                } else {
                    errorBlock("Wrong data while getting movies")
                }
            case .failure(_):
                errorBlock("Error de conexion getVideoMovie")
            }
        }
    }
    
    
}

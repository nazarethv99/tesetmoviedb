//
//  NetWorkingServices.swift
//  TestMoviedb
//
//  Created by Hostienda on 5/27/19.
//  Copyright © 2019 Hostienda. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper


let baseURL = "https://api.themoviedb.org/3"
let apiKey = "4f9612ab43a3972033f36207c365bff3"

class ApiError : NSObject, Mappable {
    
    var message: String!
    
    required init?(map: Map) {
        
    }
    
    override init() {
        
    }
    
    func mapping(map: Map) {
        self.message <- map["message"]
    }
}


struct Headers{
    static let contentType = "Content-Type"
    static let applicationJson = "application/json"
    static let applicationUrlencoded = "application/x-www-form-urlencoded"
    static let Authorization = "Authorization"
}

struct Api {
    
    struct Movie {
        static let popularMovies = "\(baseURL)/movie/popular?api_key=\(apiKey)&language=\(Locale.current.languageCode ?? "es")&page=1"
        static let topRatedMovies = "\(baseURL)/movie/top_rated?api_key=\(apiKey)&language=\(Locale.current.languageCode ?? "es")&page=1"
        static let upComingMovies = "\(baseURL)/movie/upcoming?api_key=\(apiKey)&language=\(Locale.current.languageCode ?? "es")&page=1"
    }
    
    struct Detail {
        static let detailUrl = "\(baseURL)/movie/"
        static let detailRest = "?api_key=\(apiKey)&language=\(Locale.current.languageCode ?? "es")"
    }
    
    struct image {
        static let image500 = "https://image.tmdb.org/t/p/w500"
        static let image300 = "https://image.tmdb.org/t/p/w300"
        static let imageOri = "https://image.tmdb.org/t/p/original"
    }
    
    struct video {
        static let videoDetail = "\(baseURL)/movie/"
        static let videoRest = "/videos?api_key=\(apiKey)&language=\(Locale.current.languageCode ?? "es")"
    }
    
}

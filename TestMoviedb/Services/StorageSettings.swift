//
//  Storage.swift
//  TestMoviedb
//
//  Created by Hostienda on 5/30/19.
//  Copyright © 2019 Hostienda. All rights reserved.
//

import Foundation
import ObjectMapper

class Storage {
    fileprivate static let userKey = "MOVIE"
    
    class var movieInfo : ListMovie? {
        get {
            if let userString = UserDefaults.standard.string(forKey: userKey) {
                return Mapper<ListMovie>().map(JSONString: userString)
            }
            return  nil
        }
        set {
            UserDefaults.standard.set(newValue?.toJSONString(), forKey: userKey)
        }
    }
    
    class func removeStorage() {
        UserDefaults.standard.removeObject(forKey: userKey)
    }
}
